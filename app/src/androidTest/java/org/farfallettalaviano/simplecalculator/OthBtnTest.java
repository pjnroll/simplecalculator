package org.farfallettalaviano.simplecalculator;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.ViewMatchers.*;

@RunWith(AndroidJUnit4.class)
public class OthBtnTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    /**
     * Test the delete button on txtVal1
     */
    @Test
    public void clickDelBtnTest1 () throws Exception {
        onView(withId(R.id.btnDelete)).perform(click());

        onView(withId(R.id.txtVal1))
                .check(matches(withText(""))
        );

    }

    @Test
    public void clickDelBtnTest2 () throws Exception {
        onView(withId(R.id.btn1)).perform(click());

        onView(withId(R.id.btnDelete)).perform(click());

        onView(withId(R.id.txtVal1))
                .check(matches(withText(""))
        );
    }

    @Test
    public void clickDelBtnTest3 () throws  Exception {
        onView(withId(R.id.btn1)).perform(click());
        onView(withId(R.id.btn2)).perform(click());

        onView(withId(R.id.btnDelete)).perform(click());

        onView(withId(R.id.txtVal1))
                .check(matches(withText("1"))
        );

    }

    @Test
    public void clickDelBtnTest4 () throws  Exception {
        onView(withId(R.id.btn1)).perform(click());
        onView(withId(R.id.btn2)).perform(click());
        onView(withId(R.id.btn3)).perform(click());

        onView(withId(R.id.btnDelete)).perform(click());

        onView(withId(R.id.txtVal1))
                .check(matches(withText("12"))
        );

    }

    /**
     * Test the delete button on txtVal2
     */
    @Test
    public void clickDelBtnTest5 () throws  Exception {
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());

        onView(withId(R.id.btnDelete)).perform(click());

        onView(withId(R.id.txtVal2))
                .check(matches(withText(""))
        );

    }

    @Test
    public void clickDelBtnTest6 () throws  Exception {
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());

        onView(withId(R.id.btn1)).perform(click());

        onView(withId(R.id.btnDelete)).perform(click());

        onView(withId(R.id.txtVal2))
                .check(matches(withText(""))
        );

    }

    @Test
    public void clickDelBtnTest7 () throws  Exception {
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());

        onView(withId(R.id.btn1)).perform(click());
        onView(withId(R.id.btn2)).perform(click());

        onView(withId(R.id.btnDelete)).perform(click());

        onView(withId(R.id.txtVal2))
                .check(matches(withText("1"))
        );

    }

    @Test
    public void clickDelBtnTest8 () throws Exception {
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());

        onView(withId(R.id.btn1)).perform(click());
        onView(withId(R.id.btn2)).perform(click());
        onView(withId(R.id.btn3)).perform(click());

        onView(withId(R.id.btnDelete)).perform(click());

        onView(withId(R.id.txtVal2))
                .check(matches(withText("12"))
        );
    }

    /**
     * Test the delete button
     */
    @Test
    public void pressDelBtnTest1 () throws Exception {
        onView(withId(R.id.btn7)).perform(click());
        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.btnDelete)).perform(longClick());

        onView(withId(R.id.txtVal1))
                .check(matches(withText(""))
        );
    }

    @Test
    public void pressDelBtnTest2 () throws Exception {
        onView(withId(R.id.btn5)).perform(click());
        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.btnMultiply)).perform(click());

        onView(withId(R.id.btnDelete)).perform(longClick());

        onView(withId(R.id.txtVal1))
                .check(matches(withText(""))
        );

        onView(withId(R.id.txtOp))
                .check(matches(withText(""))
        );
    }

    @Test
    public void pressDelBtnTest3 () throws Exception {
        onView(withId(R.id.btn5)).perform(click());
        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.btnMultiply)).perform(click());

        onView(withId(R.id.btn3)).perform(click());

        onView(withId(R.id.btnDelete)).perform(longClick());

        onView(withId(R.id.txtVal2))
                .check(matches(withText(""))
        );

        onView(withId(R.id.txtOp))
                .check(matches(withText(""))
        );

        onView(withId(R.id.txtVal1))
                .check(matches(withText(""))
        );
    }

    @Test
    public void pressDelBtnTest4 () throws Exception {
        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.btnMultiply)).perform(click());

        onView(withId(R.id.btn3)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());

        onView(withId(R.id.txtResult))
                .check(matches(withText("15.0"))
        );

        onView(withId(R.id.btnDelete)).perform(longClick());

        onView(withId(R.id.txtResult))
                .check(matches(withText(""))
        );

        onView(withId(R.id.txtVal2))
                .check(matches(withText(""))
        );

        onView(withId(R.id.txtOp))
                .check(matches(withText(""))
        );

        onView(withId(R.id.txtVal1))
                .check(matches(withText(""))
        );
    }

    /**
     * Test the dot button
     */
    @Test
    public void clickDotBtnTest1 () throws Exception {
        onView(withId(R.id.txtVal1)).perform(click());

        onView(withId(R.id.btn1)).perform(click());

        onView(withId(R.id.btnDot)).perform(click());

        onView(withId(R.id.btn2)).perform(click());
        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.txtVal1))
                .check(matches(withText("1.25"))
        );
    }

    @Test
    public void clickDotBtnTest2 () throws Exception {
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());

        onView(withId(R.id.btn1)).perform(click());

        onView(withId(R.id.btnDot)).perform(click());

        onView(withId(R.id.btn2)).perform(click());
        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.txtVal2))
                .check(matches(withText("1.25"))
        );
    }

    /**
     * Test Plus/Minus button
     */
    @Test
    public void clickPlusMinusBtnTest1 () throws Exception {
        onView(withId(R.id.btn1)).perform(click());
        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.btnPlusMinus)).perform(click());

        onView(withId(R.id.txtVal1))
                .check(matches(withText("-15.0"))
        );
    }

    @Test
    public void clickPlusMinusBtnTest2 () throws Exception {
        onView(withId(R.id.btn3)).perform(click());
        onView(withId(R.id.btn1)).perform(click());

        onView(withId(R.id.btnPlusMinus)).perform(click());
        onView(withId(R.id.btnPlusMinus)).perform(click());

        onView(withId(R.id.txtVal1))
                .check(matches(withText("31.0"))
        );
    }

    @Test
    public void clickPlusMinusBtnTest3 () throws Exception {
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());

        onView(withId(R.id.btn9)).perform(click());
        onView(withId(R.id.btn0)).perform(click());

        onView(withId(R.id.btnPlusMinus)).perform(click());

        onView(withId(R.id.txtVal2))
                .check(matches(withText("-90.0"))
        );
    }

    @Test
    public void clickPlusMinusTest4 () throws Exception {
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());

        onView(withId(R.id.btn1)).perform(click());
        onView(withId(R.id.btn5)).perform(click());
        onView(withId(R.id.btn2)).perform(click());
        onView(withId(R.id.btnDot)).perform(click());
        onView(withId(R.id.btn3)).perform(click());
        onView(withId(R.id.btn8)).perform(click());

        onView(withId(R.id.btnPlusMinus)).perform(click());
        onView(withId(R.id.btnPlusMinus)).perform(click());

        onView(withId(R.id.txtVal2))
                .check(matches(withText("152.38"))
        );
    }
}