package org.farfallettalaviano.simplecalculator;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.widget.Button;

import org.farfallettalaviano.simplecalculator.utils.Utils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertTrue;

/**
 * Test the behavious of the Utils' class
 */
@RunWith(AndroidJUnit4.class)
public class UtilsTest {

    /**
     * Create an instance of the Activity under test
     */
    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    /**
     * Asserts that a button instance obtained with the Utils.findViewById(Activity, int) method, returns
     * a value as valid as the value returned by the Activity.findViewById(int) method
     */
    @Test
    public void findViewByIdTest() {
        Button mButton1 = Utils.findViewById(mainActivityActivityTestRule.getActivity(), R.id.btn0);
        Button mButton2 = (Button) mainActivityActivityTestRule.getActivity().findViewById(R.id.btn0);
        assertTrue(mButton1.getClass() == mButton2.getClass());
    }
}