package org.farfallettalaviano.simplecalculator;

import android.support.test.espresso.UiController;
import android.support.test.espresso.ViewAction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;
import android.widget.TextView;

import org.hamcrest.Matcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.*;
import static android.support.test.espresso.action.ViewActions.*;
import static android.support.test.espresso.assertion.ViewAssertions.*;
import static android.support.test.espresso.matcher.RootMatchers.isPlatformPopup;
import static android.support.test.espresso.matcher.ViewMatchers.*;

/**
 * Test that the copy/paste function works properly
 */
@RunWith(AndroidJUnit4.class)
public class CopyPasteTest {
    private String stringHolder;

    /**
     * Create an instance of the Activity under test
     */
    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void toPasteTest1 () throws Exception {
        onView(withId(R.id.txtVal1)).perform(longClick());

        onView(withText(R.string.copy)).inRoot(isPlatformPopup()).perform(click());

        onView(withId(R.id.txtVal2)).perform(longClick());

        onView(withText(R.string.paste)).inRoot(isPlatformPopup()).perform(click());

        String mTxtVal1 = getText(withId(R.id.txtVal1));

        onView(withId(R.id.txtVal2))
                .check(matches(withText(mTxtVal1))
        );
    }

    @Test
    public void toPasteTest2 () throws Exception {
        onView(withId(R.id.btn1)).perform(click());
        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.txtVal1)).perform(longClick());

        onView(withText(R.string.copy)).inRoot(isPlatformPopup()).perform(click());

        onView(withId(R.id.txtVal2)).perform(longClick());

        onView(withText(R.string.paste)).inRoot(isPlatformPopup()).perform(click());

        String mTxtVal1 = getText(withId(R.id.txtVal1));

        onView(withId(R.id.txtVal2))
                .check(matches(withText(mTxtVal1))
        );
    }

    @Test
    public void toPasteTest3 () throws Exception {
        onView(withId(R.id.btn8)).perform(click());

        onView(withId(R.id.btnMultiply)).perform(click());

        onView(withId(R.id.btn4)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());

        String mTxtResult = getText(withId(R.id.txtResult));

        onView(withId(R.id.txtResult)).perform(longClick());

        onView(withText(R.string.copy)).inRoot(isPlatformPopup()).perform(click());

        onView(withId(R.id.btnDelete)).perform(longClick());

        onView(withId(R.id.txtVal1)).perform(longClick());

        onView(withText(R.string.paste)).inRoot(isPlatformPopup()).perform(click());

        onView(withId(R.id.txtVal1))
                .check(matches(withText(mTxtResult))
        );
    }

    @Test
    public void toPasteTest4 () throws Exception {
        onView(withId(R.id.btn9)).perform(click());

        onView(withId(R.id.btnDivide)).perform(click());

        onView(withId(R.id.btn5)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());

        String mTxtResult = getText(withId(R.id.txtResult));

        onView(withId(R.id.txtResult)).perform(longClick());

        onView(withText(R.string.copy)).inRoot(isPlatformPopup()).perform(click());

        onView(withId(R.id.btnDelete)).perform(longClick());

        onView(withId(R.id.txtVal2)).perform(longClick());

        onView(withText(R.string.paste)).inRoot(isPlatformPopup()).perform(click());

        onView(withId(R.id.txtVal2))
                .check(matches(withText(mTxtResult))
        );
    }

    private String getText(final Matcher<View> matcher) {
        stringHolder = null;
        onView(matcher).perform(new ViewAction() {
            @Override
            public Matcher<View> getConstraints() {
                return isAssignableFrom(TextView.class);
            }

            @Override
            public String getDescription() {
                return "getting text from a TextView";
            }

            @Override
            public void perform(UiController uiController, View view) {
                TextView tv = (TextView)view; //Save, because of check in getConstraints()
                stringHolder = tv.getText().toString();
            }
        });
        return stringHolder;
    }
}