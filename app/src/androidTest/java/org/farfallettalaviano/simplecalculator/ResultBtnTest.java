package org.farfallettalaviano.simplecalculator;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Test the behaviour of the Result button
 */
@RunWith(AndroidJUnit4.class)
public class ResultBtnTest {

    /**
     * Create an instance of the Activity under test
     */
    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Test
    public void noValues() throws Exception {
        onView(withId(R.id.btnResult)).perform(click());
        onView(withId(R.id.txtResult))
                .check(matches(withText(R.string.err_missing_everything))
        );

    }

    @Test
    public void firstValue() throws Exception {
        onView(withId(R.id.btn0)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());
        onView(withId(R.id.txtResult))
                .check(matches(withText(R.string.err_missing_op))
        );
    }

    @Test
    public void opValue() throws Exception {
        onView(withId(R.id.btnAdd)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());
        onView(withId(R.id.txtResult))
                .check(matches(withText(R.string.err_missing_val))
        );
    }

    @Test
    public void secondValue() throws Exception {
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());  // It performs the click twice because it doesn't always get the focus
        onView(withId(R.id.btn0)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());
        onView(withId(R.id.txtResult))
                .check(matches(withText(R.string.err_missing_op))
        );
    }

    @Test
    public void firstAndOpValues() throws Exception {
        onView(withId(R.id.btn0)).perform(click());
        onView(withId(R.id.btnAdd)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());
        onView(withId(R.id.txtResult))
                .check(matches(withText(R.string.err_missing_val))
        );
    }

    @Test
    public void firstAndSecondValues() throws Exception {
        onView(withId(R.id.btn0)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.btn1)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());
        onView(withId(R.id.txtResult))
                .check(matches(withText(R.string.err_missing_op))
        );
    }

    @Test
    public void opAndSecondValues() throws Exception {
        onView(withId(R.id.txtOp)).perform(click());
        onView(withId(R.id.btnAdd)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.txtVal2)).perform(click());
        onView(withId(R.id.btn0)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());
        onView(withId(R.id.txtResult))
                .check(matches(withText(R.string.err_missing_val))
        );
    }

    @Test
    public void allFilled() throws Exception {
        onView(withId(R.id.btn0)).perform(click());
        onView(withId(R.id.btnAdd)).perform(click());
        onView(withId(R.id.btn1)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());
        onView(withId(R.id.txtResult))
                .check(matches(withText("1.0"))
        );
    }
}