package org.farfallettalaviano.simplecalculator;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;

import org.junit.runner.RunWith;
import org.junit.Rule;
import org.junit.Test;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;

/**
 * Test that the operations work properly
 */
@RunWith(AndroidJUnit4.class)
public class OperationsViewTest {

    @Rule
    public ActivityTestRule<MainActivity> mainActivityActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    /**
     * Test the operations on txtResult
     */
    @Test
    public void txtResultTest1 () throws Exception {
        onView(withId(R.id.txtResult))
                .check(matches(withText(""))
        );
    }

    @Test
    public void txtResultAddTest () throws Exception {
        onView(withId(R.id.btn2)).perform(click());
        onView(withId(R.id.btnAdd)).perform(click());
        onView(withId(R.id.btn2)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());

        onView(withId(R.id.txtResult))
                .check(matches(withText("4.0"))
        );
    }

    @Test
    public void txtResultSubTest () throws Exception {
        onView(withId(R.id.btn4)).perform(click());
        onView(withId(R.id.btnSubtract)).perform(click());
        onView(withId(R.id.btn2)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());

        onView(withId(R.id.txtResult))
                .check(matches(withText("2.0"))
        );
    }

    @Test
    public void txtResultMulTest () throws Exception {
        onView(withId(R.id.btn5)).perform(click());
        onView(withId(R.id.btnMultiply)).perform(click());
        onView(withId(R.id.btn2)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());

        onView(withId(R.id.txtResult))
                .check(matches(withText("10.0"))
        );
    }

    @Test
    public void txtResultDivTest () throws Exception {
        onView(withId(R.id.btn1)).perform(click());
        onView(withId(R.id.btn0)).perform(click());
        onView(withId(R.id.btnDivide)).perform(click());
        onView(withId(R.id.btn2)).perform(click());

        onView(withId(R.id.btnResult)).perform(click());

        onView(withId(R.id.txtResult))
                .check(matches(withText("5.0"))
        );
    }
}