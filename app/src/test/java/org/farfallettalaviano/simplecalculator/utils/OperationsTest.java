package org.farfallettalaviano.simplecalculator.utils;

import org.junit.Test;

import static java.lang.Math.E;
import static org.junit.Assert.assertEquals;

public class OperationsTest {
    private final static double delta = E-8d;   // it matches with the epsilon-machine

    @Test
    public void sumTest() {
        assertEquals(4.0d, Operations.add(2.0d,2.0d), delta);
        assertEquals(0.0d, Operations.add(2.0d,-2.0d), delta);
        assertEquals(0.0d, Operations.add(-2.0d,2.0d), delta);
        assertEquals(-4.0d, Operations.add(-2.0d, -2.0d), delta);
    }

    @Test (expected = AssertionError.class)
    public void sumTest2() {
        assertEquals(9.8888888888d, Operations.add(2.3333333333d,7.5555555555d), delta);
        assertEquals(7.888888888d, Operations.add(5.333333333d,2.555555555d), delta);
        assertEquals(14.246913578d, Operations.add(5.123456789d,9.123456789d), delta);
    }
    @Test
    public void subTest() {
        assertEquals(10.0d, Operations.subtract(20.0d, 10.0d), delta);
        assertEquals(30.0d, Operations.subtract(20.0d, -10.0d), delta);
        assertEquals(-30.0d, Operations.subtract(-20.0d, 10.0d), delta);
        assertEquals(-10.0d, Operations.subtract(-20.0d, -10.0d), delta);
    }

    @Test (expected = AssertionError.class)
    public void subTest2() {
        assertEquals(0.0025d,  Operations.subtract(1.0025d, 1.0d), delta);
        assertEquals(4.678888888d, Operations.subtract(9.888888888d, 5.21d), delta);
        assertEquals(5.000000004d,  Operations.subtract(5.000000005d, 0.000000001d), delta);
    }


    @Test
    public void mulTest() {
        assertEquals(4.0d, Operations.multiply(2.0d, 2.0d), delta);
        assertEquals(-4.0d, Operations.multiply(2.0d, -2.0d), delta);
        assertEquals(-4.0d, Operations.multiply(-2.0d, 2.0d), delta);
        assertEquals(4.0d, Operations.multiply(-2.0d, -2.0d), delta);
    }

    @Test (expected = AssertionError.class)
    public void mulTest2() {
        assertEquals(5d, Operations.multiply(2.5d, 2d), delta);
        assertEquals(40.902187082684479d, Operations.multiply(5.65218902d, 7.23652145d), delta);
        assertEquals(32.0000008400000027d, Operations.multiply(8.00000003, 4.00000009), delta);

    }

    @Test
    public void divTest() {
        assertEquals(4.0d, Operations.divide(8.0d, 2.0d), delta);
        assertEquals(-4.0d, Operations.divide(8.0d, -2.0d), delta);
        assertEquals(-4.0d, Operations.divide(-8.0d, 2.0d), delta);
        assertEquals(4.0d, Operations.divide(-8.0d, -2.0d), delta);
    }

    @Test (expected = AssertionError.class)
    public void divTest2() {
        assertEquals(1.673046934d, Operations.divide(8.36523467, 5d), delta);
        assertEquals(1.6543209876543209876543209876543d, Operations.divide(5.36d, 3.24d), delta);
        assertEquals(3.2877697841726618705035971223022d, Operations.divide(0.3656d, 0.1112d), delta);
    }

    @Test(expected = NumberFormatException.class)
    public void divideByZeroTest() {
        Operations.divide(1.0d, 0.0d);
    }
}