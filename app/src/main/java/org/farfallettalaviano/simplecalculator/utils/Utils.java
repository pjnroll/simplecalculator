package org.farfallettalaviano.simplecalculator.utils;

import android.app.Activity;
import android.view.View;

public class Utils {
    /**
     * This method let to avoid casting every time the Views to their specific sub-classes;
     * @param act the activity that contains the View
     * @param viewId the id of the view to be returned
     * @param <T> the class parameter to be returned
     * @return the casted view
     */
    public static <T extends View> T findViewById(Activity act, int viewId) {
        View containerView = act.getWindow().getDecorView();
        View toRet = containerView.findViewById(viewId);

        return (T) toRet;
    }
}
